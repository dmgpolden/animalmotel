from typing import List, Optional
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

import crud, models, schemas
from database import SessionLocal, engine

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# class Item(BaseModel):
#     name: str
#     price: float
#     is_offer: Optional[bool] = None
#

@app.get("/animals/", response_model=list[schemas.Animal])
def read_animals(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    animals = crud.get_animals(db, skip=skip, limit=limit)
    return animals
