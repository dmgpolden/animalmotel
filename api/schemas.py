from datetime import date
from typing import Any, Dict, Type, List, Optional
from uuid import UUID

from pydantic import BaseModel, Field, json


class OrmBase(BaseModel):

    class Config:
        orm_mode = True


class ItemBase(OrmBase):
    id: UUID


class ItemCreate(BaseModel):
    pass


class Item(ItemBase):
    name: str


class Payment(ItemBase):
    date: date
    date_from: date
    date_to: date
    amount: int


class Breed(OrmBase):
    name: str


class Live(OrmBase):
    id: Optional[int] = None
    status: Optional[int] = None
    date_begin: date
    date_end: Optional[date] = None
    price: Optional[int] = None


class Animal(Item):
    birth: Optional[date] = None
    gender: int
    breed: Breed
    live: Optional[List[Live]] = None
    payments: Optional[List[Payment]] = None
    # data: json


# this is equivalent to json.dumps(MainModel.schema(), indent=2):
print(Animal.schema_json(indent=2))
print(Live.schema_json(indent=2))
print(Breed.schema_json(indent=2))


class UserBase(BaseModel):
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool
    items: List[Item] = []

    class Config:
        orm_mode = True
