from sqlalchemy.orm import Session
import models, schemas


def get_animal(db: Session, animal_id: int):
    return db.query(models.Animal).filter(models.animal.id == animal_id).first()


def get_animals(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Animal).offset(skip).limit(limit).all()
