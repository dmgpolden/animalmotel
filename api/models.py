from sqlalchemy import Boolean,\
    Column,\
    ForeignKey,\
    Integer,\
    String,\
    Date,\
    JSON
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from database import Base


class Breed(Base):
    __tablename__ = 'breed'

    id = Column(UUID, primary_key=True)
    name = Column(String(255))


class Live(Base):
    __tablename__ = 'live'

    id = Column(Integer, primary_key=True)
    status = Column(Integer)
    date_begin = Column(Date)
    date_end = Column(Date)
    animal_id = Column(UUID, ForeignKey('animal.id'))
    room_id = Column(UUID, ForeignKey('room.id'))
    price = Column(Integer)
    room = relationship("Room", lazy="joined", uselist=False)


class Room(Base):
    __tablename__ = 'room'

    id = Column(UUID, primary_key=True)
    name = Column(String(255))


class Person(Base):
    __tablename__ = 'person'

    id = Column(UUID, primary_key=True)
    name_first = Column(String(1000), nullable=False)
    name_middle = Column(String(1000), nullable=False)
    name_last = Column(String(1000), nullable=False)

    # animals = relationship("Animal")


class Payment(Base):
    __tablename__ = 'payment'

    id = Column(UUID, primary_key=True)
    date = Column(Date)
    date_from = Column(Date)
    date_to = Column(Date)
    amount = Column(Integer)
    # animals = relationship("Animal")
    animal_id = Column(UUID, ForeignKey('animal.id'))


class Animal(Base):
    __tablename__ = 'animal'
    id = Column(UUID, primary_key=True)
    name = Column(String(1000), nullable=False)
    birth = Column(Date)
    gender = Column(Integer, name="gender")
    breed_id = Column(UUID, ForeignKey('breed.id'))
    breed = relationship("Breed", lazy="joined")
    # data = Column(JSON)

    # owner_id = Column(UUID, ForeignKey('person.id'))
    # owner = relationship("Person", lazy="joined")
    live = relationship("Live")
    payments = relationship("Payment", order_by="desc(Payment.date)")
