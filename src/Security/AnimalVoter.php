<?php

namespace App\Security;

use App\Entity\Animal;
use App\Entity\User;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class AnimalVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';
    
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    protected function supports(string $attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        // only vote on `Animal` objects
        if (!$subject instanceof Animal) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }
        
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is Animal object, thanks to `supports()`
        /** @var Animal $animal */
        $animal = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($animal, $user);
            case self::EDIT:
                return $this->canEdit($animal, $user);
        }

        throw new LogicException('This code should not be reached!');
    }

    private function canView(Animal $animal, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($animal, $user)) {
            return true;
        }

        // the animal object could have, for example, a method `isPrivate()`
        //return !$animal->isPrivate();
    }

    private function canEdit(animal $animal, User $user)
    {
        // this assumes that the animal object has a `getOwner()` method
        return $user === $animal->getOwner();
    }
}
