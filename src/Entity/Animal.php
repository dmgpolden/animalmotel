<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 */
class Animal
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\OneToOne(targetEntity=Live::class, mappedBy="animal")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    protected $headshot;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Breed", inversedBy="animals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $breed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birth;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\File", inversedBy="animals")
     */
    private $files;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File", inversedBy="animal", cascade={"persist", "remove"})
     */
    private $avatar;

    /**
    // * @ORM\ManyToMany(targetEntity="App\Entity\Person", mappedBy="animals")
     */
   // private $people;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="animals")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Task::class, mappedBy="animal")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $tasks;

    /**
     * @ORM\OneToOne(targetEntity=Live::class, mappedBy="animal")
     */
    private $live;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="animal")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $payments;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $data = [
        'size' => null,
        'weight' => null
        ];

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $gender;


    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    public function setHeadshot(File $file = null)
    {
        $this->headshot = $file;
    }

    public function getHeadshot()
    {
        return $this->headshot;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBreed(): ?Breed
    {
        return $this->breed;
    }

    public function setBreed(?Breed $breed): self
    {
        $this->breed = $breed;

        return $this;
    }

    public function getImages(): Collection
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('asType', File::TYPE_IMAGE));
        return $this->files->matching($criteria);
    }
    
    public function getDocuments(): Collection
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('asType', File::TYPE_DOCUMENT));
        return $this->files->matching($criteria);
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getBirth(): ?\DateTimeInterface
    {
        return $this->birth;
    }

    public function setBirth(?\DateTimeInterface $birth): self
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }

        return $this;
    }

    public function getAvatar(): ?File
    {
        return $this->avatar;
    }

    public function setAvatar(?File $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getOwner(): ?Person
    {
        return $this->owner;
    }

    public function setOwner(?Person $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setAnimal($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getAnimal() === $this) {
                $task->setAnimal(null);
            }
        }

        return $this;
    }

    public function getLive(): ?Live
    {
        return $this->live;
    }

    public function setLive(Live $live): self
    {
        $this->live = $live;

        // set the owning side of the relation if necessary
        if ($live->getAnimal() !== $this) {
            $live->setAnimal($this);
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {   
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setAnimal($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getAnimal() === $this) {
                $payment->setAnimal(null);
            }
        }

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData($datas)
    {
        foreach ($datas as $key => $value) {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function getGenderName(): string
    {
        if ( $this->gender == 1 ) {
            return 'male' ;
        }elseif ($this->gender == 2 ){
            return 'female' ;
        }else{
            return 'no';
        }
            
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }
}
