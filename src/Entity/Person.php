<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameLast;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameFirst;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameMiddle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="person", orphanRemoval=true, cascade={"persist"})
     */
    private $contacts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="persons")
     */
    private $person;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Person", mappedBy="person")
     */
    private $persons;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Animal", mappedBy="owner")
     */
    private $animals;

    /**
     * @ORM\OneToMany(targetEntity=Task::class, mappedBy="person")
     */
    private $tasks;
    
    /**
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->persons = new ArrayCollection();
        $this->animals = new ArrayCollection();
        $this->tasks = new ArrayCollection();
    }

    public function __toString()
    {
        return 'Person';
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        $name = $this->nameFirst ?? '' ;
        $name .= ' ' . $this->nameMiddle ?? '';
        $name .= ' ' . $this->nameLast ?? ''  ;
        
        return $name;
    }

    public function getNameLast(): ?string
    {
        return $this->nameLast;
    }

    public function setNameLast(string $nameLast): self
    {
        $this->nameLast = $nameLast;

        return $this;
    }

    public function getNameFirst(): ?string
    {
        return $this->nameFirst;
    }

    public function setNameFirst(string $nameFirst): self
    {
        $this->nameFirst = $nameFirst;

        return $this;
    }

    public function getNameMiddle(): ?string
    {
        return $this->nameMiddle;
    }

    public function setNameMiddle(?string $nameMiddle): self
    {
        $this->nameMiddle = $nameMiddle;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPerson($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getPerson() === $this) {
                $contact->setPerson(null);
            }
        }

        return $this;
    }

    public function getPerson(): ?self
    {
        return $this->person;
    }

    public function setPerson(?self $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function addPerson(self $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
            $person->setPerson($this);
        }

        return $this;
    }

    public function removePerson(self $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            // set the owning side to null (unless already changed)
            if ($person->getPerson() === $this) {
                $person->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Animal[]
     */
    public function getAnimals(): Collection
    {
        return $this->animals;
    }

    public function addAnimal(Animal $animal): self
    {
        if (!$this->animals->contains($animal)) {
            $this->animals[] = $animal;
            $animal->setOwner($this);
        }

        return $this;
    }

    public function removeAnimal(Animal $animal): self
    {
        if ($this->animals->contains($animal)) {
            $this->animals->removeElement($animal);
            // set the owning side to null (unless already changed)
            if ($animal->getOwner() === $this) {
                $animal->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setPerson($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getPerson() === $this) {
                $task->setPerson(null);
            }
        }

        return $this;
    }

}
