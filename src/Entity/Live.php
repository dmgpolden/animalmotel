<?php

namespace App\Entity;

use App\Repository\LiveRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LiveRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="search_date", columns={"date_begin", "date_end"})})

 */
class Live
{
     const STATUS_FREE = 1;
     const STATUS_PLAN = 2;
     const STATUS_DONE = 3;
     /**
      * Getter for StatusChoices 
      *
      * @return string
      */
     public function getStatusChoices()
     {
         return [
              'free'=>self::STATUS_FREE,
              'plan'=>self::STATUS_PLAN,
              'done'=>self::STATUS_DONE,
         ];
     }
     
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateBegin;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="lives", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    /**
     * @ORM\OneToOne(targetEntity=Animal::class, inversedBy="live", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $animal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price;
    
    private $dayPrice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateBegin(): ?\DateTimeInterface
    {
        return $this->dateBegin;
    }

    public function setDateBegin(\DateTimeInterface $dateBegin): self
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }
    
    public function getStatusName(): ?string
    {
        $statuses=array_flip($this->getStatusChoices());
        return $statuses[$this->status];
    }


    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getAnimal(): ?Animal
    {
        return $this->animal;
    }

    public function setAnimal(Animal $animal): self
    {
        $this->animal = $animal;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }
    
    public function getDayPrice(): ?int
    {
        return $this->price / 31;
    }

}
