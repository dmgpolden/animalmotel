<?php

namespace App\Form;

use App\Entity\Animal;
use App\Entity\Live;
use App\Entity\Room;
use App\Repository\AnimalRepository;
use App\Repository\RoomRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LiveType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateBegin', DateType::class,[
                'widget' => 'single_text',
                ])
            ->add('dateEnd', DateType::class,[
                'widget' => 'single_text',
                'required' => false,
                ])
                ->add('status', ChoiceType::class, [
                    'choices' => Live::getStatusChoices(),
                    'expanded' => false,
				    'placeholder' => ' -- ',
                ])
            ->add('animal', EntityType::class, [
                    'class' => Animal::class,
                    'query_builder' => function (AnimalRepository $ar) {
					    return $ar->createQueryBuilder('a')
						->orderBy('a.name', 'ASC')
                        ;
				    },
                    'choice_label' => function($animal) { return $animal->getName() . ' ' .  $animal->getBreed()->getName() ; }, //', 'breed.name',
                    'label' => false,
				    'placeholder' => 'animal',
                    ])
            ->add('price')
            ->add('room', EntityType::class, [
                    'class' => Room::class,
				    'query_builder' => function (RoomRepository $er) {
					    return $er->createQueryBuilder('r')
						->orderBy('r.place', 'ASC')
                        ->addOrderBy('r.name', 'ASC')
                        ;
				    },
                    'choice_label' => function ($r) {
                        return  $r->getName() . ' ' . $r->getPlace()->getName();
                        },
                    'label' => false,
				    'placeholder' => 'room',
                    ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Live::class,
        ]);
    }
}
