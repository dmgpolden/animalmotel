<?php

namespace App\Form;

use App\Entity\Person;
use App\Repository\PersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Form\ContactType;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameLast')
            ->add('nameFirst')
            ->add('nameMiddle')
            ->add('contacts', CollectionType::class, [
                'entry_type' => ContactType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                ])
            ->add('person', EntityType::class, [
				'class' => Person::class,
				'query_builder' => function (PersonRepository $er) {
					return $er->createQueryBuilder('p')
                        ->orderBy('p.nameFirst', 'ASC');
						
				},
				'choice_label' => 'name',
				'placeholder' => ' -- ',
                'required' => true,
                'attr' => [ 'class' => 'uk-autocomplete uk-form',
                            'data-uk-autocomplete' => "{source:'my-autocomplete.json'}",
                          ],
			])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
