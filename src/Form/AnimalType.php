<?php

namespace App\Form;

use App\Entity\Animal;
use App\Entity\Breed;
use App\Entity\Person;
use App\Repository\BreedRepository;
use App\Repository\PersonRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('gender', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, [
                'placeholder' => ' -- ',
                'choices' => ['male' => 1, 'female' => 2],
            ])
            ->add('breed', EntityType::class, [
				'class' => Breed::class,
				'query_builder' => function (BreedRepository $er) {
					return $er->createQueryBuilder('b')
						->orderBy('b.name', 'ASC');
				},
				'choice_label' => 'name',
				'placeholder' => ' -- ',
                'required' => true,
                'attr' => [ 'class' => 'uk-autocomplete uk-form',
                            'data-uk-autocomplete' => "{source:'my-autocomplete.json'}",
                          ],
			]
            )
            ->add('data', CollectionType::class)
            ->add('owner', EntityType::class, [
				'class' => Person::class,
				'query_builder' => function (PersonRepository $er) {
					return $er->createQueryBuilder('p')
                        ->orderBy('p.nameFirst', 'ASC');
						
				},
				'choice_label' => 'name',
				'placeholder' => ' -- ',
                'required' => true,
                'attr' => [ 'class' => 'uk-autocomplete uk-form',
                            'data-uk-autocomplete' => "{source:'my-autocomplete.json'}",
                          ],
			]
            )
            /*
            ->add('live', EntityType::class, [
                    'class' => Live::class,
				    'query_builder' => function (RoomRepository $er) {
					    return $er->createQueryBuilder('r')
						->orderBy('r.place', 'ASC')
                        ->addOrderBy('r.name', 'ASC')
                        ;
				    },
                    'choice_label' => function ($r) {
                        return  $r->getName() . ' ' . $r->getPlace()->getName();
                        },
                    'label' => false,
				    'placeholder' => 'room',
                    ])
            */
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Animal::class,
        ]);
    }
}
