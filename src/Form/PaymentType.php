<?php

namespace App\Form;

use App\Entity\Animal;
use App\Entity\Payment;
use App\Repository\AnimalRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('animal', EntityType::class, [
                    'class' => Animal::class,
                    'query_builder' => function (AnimalRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->orderBy('a.name', 'ASC');
                    },
                    'choice_label' => 'name',
                    'placeholder' => 'animal',
                    'attr' => [ 'class' => 'uk-autocomplete uk-form',
                                'data-uk-autocomplete' => "{source:'my-autocomplete.json'}",

                              ],
                    ])
            ->add('date', DateType::class,[
                'widget' => 'single_text',
                ]) 
            ->add('amount')
            ->add('dateFrom', DateType::class,[
                'widget' => 'single_text',
                ])
            ->add('dateTo', DateType::class,[
                'widget' => 'single_text',
                ]) 
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Payment::class,
        ]);
    }
}
