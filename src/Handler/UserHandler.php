<?php

namespace App\Handler;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserHandler 
{
    private $passwordEncoder;
    private $manager;

/*
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $manager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->manager= $manager;
    }
 */
    public function load($email, $password)
    {
        $user = new User();
        
        $user->setEmail($email);
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $password
        ));
        
        $this->manager->persist($user);

        $this->manager->flush();
    }
}
