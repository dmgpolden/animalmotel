<?php

namespace App\Controller;

use App\Entity\ContactType;
use App\Form\ContactTypeType;
use App\Repository\ContactTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contacttype")
 */
class ContactTypeController extends AbstractController
{
    /**
     * @Route("/", name="contacttype_index", methods={"GET"})
     */
    public function index(ContactTypeRepository $contactTypeRepository): Response
    {
        return $this->render('contact_type/index.html.twig', [
            'contact_types' => $contactTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="contacttype_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $contactType = new ContactType();
        $form = $this->createForm(ContactTypeType::class, $contactType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactType);
            $entityManager->flush();

            return $this->redirectToRoute('contacttype_index');
        }

        return $this->render('contact_type/new.html.twig', [
            'contact_type' => $contactType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contacttype_show", methods={"GET"})
     */
    public function show(ContactType $contactType): Response
    {
        return $this->render('contact_type/show.html.twig', [
            'contact_type' => $contactType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="contacttype_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ContactType $contactType): Response
    {
        $form = $this->createForm(ContactTypeType::class, $contactType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contacttype_index');
        }

        return $this->render('contact_type/edit.html.twig', [
            'contact_type' => $contactType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contacttype_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ContactType $contactType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contactType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contactType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contacttype_index');
    }
}
