<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Animal;
use App\Entity\Person;
use App\Form\PersonType;
use App\Form\AnimalType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ContactTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("/person")
 */
class PersonController extends AbstractController
{
    /**
     * @Route("/", name="person_index", methods={"GET"})
     */
    public function index(PersonRepository $personRepository): Response
    {
        return $this->render('person/index.html.twig', [
            'people' => $personRepository->findByPerson(),
        ]);
    }

    /**
     * @Route("/new", name="person_new", methods={"GET","POST"})
     */
    public function new(Request $request, ContactTypeRepository $contactTypeRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);
        $contactTypes = $contactTypeRepository->findBy(['required'=>true]);
        
        if ($form->isSubmitted() && $form->isValid()) {
           foreach($person->getContacts() as $contact ){
                $contact->setPerson($person);
                $entityManager->persist($contact);
            }
            $entityManager->persist($person);
            $entityManager->flush();

            return $this->redirectToRoute('person_sub_new', [
                'id' => $person->getId(),
            ]);
            return $this->redirectToRoute('person_index');
        }
        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}/adddog", name="person_add_dog", methods={"GET","POST"})
     */
    public function addDog(Request $request, Person $person, ContactTypeRepository $contactTypeRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $person->addAnimal($animal);
            $entityManager->persist($person);
            $entityManager->persist($animal);
            $entityManager->flush();

            return $this->redirectToRoute('person_show', ['id' => $person->getId()]);
        }

        return $this->render('animal/new.html.twig', [
            'animal' => $animal,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}/subnew", name="person_sub_new", methods={"GET","POST"})
     */
    public function subNew(Request $request, Person $person, ContactTypeRepository $contactTypeRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $subPerson = new Person();
        $subContact = new Contact();
        $contactTypes = $contactTypeRepository->findBy(['required'=>true]);
        $form = $this->createForm(PersonType::class, $subPerson);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                foreach($subPerson->getContacts() as $contact ){
                    $contact->setPerson($subPerson);
                    $entityManager->persist($contact);
                }
                $entityManager->persist($subPerson);
                $person->addPerson($subPerson);
                $entityManager->persist($person);
                $entityManager->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_show", methods={"GET"})
     */
    public function show(Person $person): Response
    {
        return $this->render('person/show.html.twig', [
            'person' => $person,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="person_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ContactTypeRepository $contactTypeRepository, Person $person): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        if(empty($person->getContacts()->toArray())){
        $contact = new Contact();
        $contactType = $contactTypeRepository->find('e6a0bfe5-9775-4f77-b92f-80be3cc7a32a');
        $contact->setType($contactType);
        $person->addContact($contact);
                $entityManager->persist($contact);
                $entityManager->persist($person);
        }
        //    dd($person->getContacts());
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        $originalContacts = new ArrayCollection();
    // Create an ArrayCollection of the current Contract objects in the database
    foreach ($person->getContacts() as $contact) {
        $originalContacts->add($contact);
    }

        if ($form->isSubmitted() && $form->isValid()) {
//        dd ($form->isSubmitted() && $form->isValid());
             foreach ($originalContacts as $contact) {
            if (false === $person->getContacts()->contains($contact)) {
                // remove the Person from the Contact
                dd($contact);
                $contact->getPerson()->removeElement($person);

                // if it was a many-to-one relationship, remove the relationship like this
                // $contact->setTask(null);

                $entityManager->persist($contact);

                // if you wanted to delete the Contact entirely, you can also do that
                 $entityManager->remove($contact);
            }
        }
            $entityManager->persist($person);
             foreach($person->getContacts() as $contact){
                $contact->setPerson($person);
             }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Person $person): Response
    {
        if ($this->isCsrfTokenValid('delete'.$person->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($person);
            $entityManager->flush();
        }

        return $this->redirectToRoute('person_index');
    }
}
