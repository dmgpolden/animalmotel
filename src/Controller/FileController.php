<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Entity\File;
use App\Service\DbUploadHandler as UploadHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/file")
 */
class FileController extends AbstractController
{
    /**
     * @Route("/ajax/{animal_id}", name="file_ajax", methods={"POST", "GET", "DELETE"})
     * @Route("/ajax/file/{file_id}", name="file_ajax_file", methods={"POST", "GET", "DELETE"})
     * @ParamConverter("animal", options={"mapping": {"animal_id"   : "id"}})
     * @ParamConverter("file", options={"mapping": {"file_id"   : "id"}})

     * @Template()
     */
    public function ajax(Animal $animal = null, File $file = null)
    {
        $upload_handler = new UploadHandler([
            'em' => $this->getDoctrine()->getManager(),
            'animal' => $animal,
            'file' => $file,
            'print_response' => false,
            'script_url' => '/file/ajax',
            'upload_dir' => $this->getParameter('app.path.upload_images'),
            'upload_url' => $this->getParameter('app.path.web_images'),
            ]);

        return new JsonResponse($upload_handler->getResponse());
    }
}
