<?php

namespace App\Controller;

use App\Entity\Animal;
use App\Entity\File;
use App\Entity\Payment;
use App\Entity\Task;
use App\Form\AnimalType;
use App\Form\PaymentType;
use App\Form\TaskType;
use App\Repository\AnimalRepository;
use App\Service\FileUploader;
use App\Service\VarGetSession;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/animal")
 */
class AnimalController extends AbstractController
{
    /**
     * @Route("/", name="animal_index", methods={"GET"})
     */
    public function index(AnimalRepository $animalRepository, Request $request, SessionInterface $session, 
            VarGetSession $varGetSession): Response
    {           
        /**
         * @var User $user
         */
        $user = $this->getUser();
        
        return $this->render('animal/index.html.twig', [
            /**
             * @var AnimalRepository Description
             */
            'animals' => $animalRepository->findFullAll($varGetSession->get('animalSearch'), $user->getPerson()),
                    ]);
    }

    /**
     * @Route("/new", name="animal_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $animal = new Animal();
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($animal);
            $entityManager->flush();
        
            if($request->request->has('SaveContinue')){
                return $this->redirectToRoute('animal_edit', [
                    'id' => $animal->getId(),
                ]);
            }

            return $this->redirectToRoute('animal_index');
        }
        return $this->render('animal/new.html.twig', [
            'animal' => $animal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="animal_show", methods={"GET"})
     */
    public function show(Animal $animal): Response
    {
        $this->denyAccessUnlessGranted('view', $animal);

        return $this->render('animal/show.html.twig', [
            'animal' => $animal,
        ]);
    }

    /**
     * @Route("/{id}/document", name="animal_document", methods={"GET","POST"})
     */
    public function document(Request $request, Animal $animal): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $file = $this->getDoctrine()
                    ->getRepository(File::class)
                    ->find($request->request->get('file_id'));
            $file->setAsType(File::TYPE_DOCUMENT);
            $file->setApproved(true);
            $entityManager->persist($file);
            $entityManager->flush();
        }catch (Exception $e){
            throw $e;
        }

        return new JsonResponse(['add'=>'ok', 'file' => $file->getName()]);
    }

    /**
     * @Route("/{id}/image", name="animal_image", methods={"GET","POST"})
     */
    public function image(Request $request, Animal $animal): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        try {
            $file = $this->getDoctrine()
                    ->getRepository(File::class)
                    ->find($request->request->get('file_id'));
            $animal->setAvatar($file);
            $entityManager->persist($animal);
            $animal->removeFile($file);
            $entityManager->flush();
        }catch (Exception $e){
            throw $e;
        }

        return new JsonResponse(['add'=>'ok', 'file' => $file->getName()]);
    }

    /**
     * @Route("/{id}/edit", name="animal_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Animal $animal, FileUploader $fileUploader): Response
    {
        $this->denyAccessUnlessGranted('edit', $animal);
        $form = $this->createForm(AnimalType::class, $animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('animal_index');
        }

        return $this->render('animal/edit.html.twig', [
            'animal' => $animal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="animal_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Animal $animal, ParameterBagInterface $params): Response
    {
        $filesystem = new Filesystem();
        $filePath = $this->getParameter('app.path.upload_images');
        $entityManager = $this->getDoctrine()->getManager();
        if ($this->isCsrfTokenValid('delete'.$animal->getId(), $request->request->get('_token'))) {
            $live = $animal->getLive();
            $files = $animal->getFiles();
            if(null !== $live ){
                $entityManager->remove($live);
            }
            
            /**
             * @var File Description File $file
             */
            foreach($files as $file){
                try {
                    $filesystem->remove($filePath . '/' . $file->getName());
                    $filesystem->remove($filePath . '/thumbnail/' . $file->getName());
                } catch (IOExceptionInterface $exception) {
                    echo "An error occurred while creating your directory at ".$exception->getPath();
                }
                $entityManager->remove($file);
            }
            $entityManager->remove($animal);
            $entityManager->flush();
        }

        return $this->redirectToRoute('animal_index');
    }

    /**
     * @Route("/{id}/task/add", name="animal_task_add", methods={"GET","POST"})
     */
    public function taskAdd(Request $request, Animal $animal): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $task = new Task();
        $task->setDate(new \DateTime());
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setAnimal($animal);
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('animal_index');
        }

        return $this->render('task/new.html.twig', [
            'animal' => $animal,
            'person' => $animal->getOwner(),
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}/payment/add", name="animal_payment_add", methods={"GET","POST"})
     */
    public function paymentAdd(Request $request, Animal $animal): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $payment = new Payment();
        $payment->setAnimal($animal);
        $form = $this->createForm(PaymentType::class, $payment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $payment->setAnimal($animal);
            $entityManager->persist($payment);
            $entityManager->flush();

            return $this->redirectToRoute('animal_index');
        }
        
        return $this->render('payment/new.html.twig', [
            'animal' => $animal,
            'person' => $animal->getOwner(),
            'payment' => $payment,
            'form' => $form->createView(),
        ]);
    }
    
    
}
