<?php

namespace App\Repository;

use App\Entity\Live;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Live|null find($id, $lockMode = null, $lockVersion = null)
 * @method Live|null findOneBy(array $criteria, array $orderBy = null)
  * @method Live[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LiveRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Live::class);
    }

    public function findAll() {
        return $this->findBy([], ['dateBegin' => 'DESC']);
    }
    
    /**
     * @return [] Returns an array of Costs
     */
    public function findCost(\DateTime $periodBegin = null, \DateTime $periodEnd = null)
    {
        $conn = $this->getEntityManager()->getConnection();
        return $conn->executeQuery($this->rawSql(), $this->getCostParams($periodBegin, $periodEnd))
                ->fetchAll();
    }
    
    /**
     * @return float Sum of Costs
     */
    public function getSumCost(\DateTime $periodBegin = null, \DateTime $periodEnd = null)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(cost) as sumCost FROM (' . $this->rawSql() .') AS sabquery';
        
        return $conn->executeQuery($sql, $this->getCostParams($periodBegin, $periodEnd))
                ->fetchColumn();
    }
    
    /**
     * 
     * @param \DateTime $periodBegin
     * @param \DateTime $periodEnd
     * @return array Return array for sql bind 
     */
    private function getCostParams(\DateTime $periodBegin  = null, \DateTime $periodEnd  = null) {
        if( null == $periodBegin){
            $periodBegin = new \DateTime();
        }
        if(null == $periodEnd){
            $periodEnd = new \DateTime();
        }
        return [
            'durationMonth' => $periodEnd->format('t'),
            'periodBegin' => $periodBegin->format('Y-m-d'),
            'periodEnd' => $periodEnd->format('Y-m-d'),
                ];
    }
    
    
    private function rawSql() {
        return
        " SELECT dayprice*duration AS cost, * FROM ("
            . "SELECT price, (price::float / :durationMonth)::float AS dayPrice, 1+periodEnd-periodBegin AS duration, * "
                . "FROM ("
                    . "SELECT CASE WHEN date_begin>:periodBegin THEN date_begin ELSE :periodBegin END AS periodBegin, CASE WHEN date_end < :periodEnd THEN date_end ELSE :periodEnd END AS periodEnd, * "
                        . "FROM live  "
                            . "LEFT JOIN animal ON ( live.animal_id = animal.id ) "
                            . "WHERE live.price IS NOT NULL "
                                . "AND (date_end>:periodBegin OR date_end IS NULL) "
                                . "AND status IN (2, 3) "
                     . ") as res ) "
            . "AS data "
        ;
    }
    
    // /**
    //  * @return Live[] Returns an array of Live objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Live
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
