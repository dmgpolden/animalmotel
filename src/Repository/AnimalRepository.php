<?php

namespace App\Repository;

use App\Entity\Animal;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use function mb_strtolower;

/**
 * @method Animal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Animal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Animal[]    findAll()
 * @method Animal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimalRepository extends ServiceEntityRepository
{
    private $em;

    public function __construct(ManagerRegistry $registry)
    {
        $this->em = $registry;
        parent::__construct($registry, Animal::class);
    }
    
    /**
     * 
     * @return Animal[] Returns an array of Animal objects
     * 
     * @param string $search Search for names
     * @param Person $person
    */
    
    public function findFullAll($animalSearch = [], Person $person = null)
    {
        $qb= $this->createQueryBuilder('a');
            $qb->select(['a', 'b', 'ava', 'l', 'o', 'r', 'pl', 'u']) // 't',  'p',
            ->leftJoin('a.avatar','ava')
            ->leftJoin('a.breed','b')
            ->leftJoin('a.live','l')
            ->leftJoin('l.room', 'r')
            ->leftJoin('r.place', 'pl')
            ->leftJoin('a.owner', 'o')
            ->leftJoin('o.user', 'u')
            //->leftJoin('a.tasks', 't')
            //->leftJoin('a.payments', 'p')
            ;
        
        if ( null !== $animalSearch){
                if(array_key_exists('search', $animalSearch) &&  '' != $animalSearch['search']){
                $search= mb_strtolower($animalSearch['search']);
                $qb->add('where', $qb->expr()->orX(
                    $qb->expr()->like($qb->expr()->lower('o.nameFirst'), $qb->expr()->literal('%' .$search . '%')),
                    $qb->expr()->like($qb->expr()->lower('o.nameLast'), $qb->expr()->literal('%' .$search . '%')),
                    $qb->expr()->like($qb->expr()->lower('a.name'), $qb->expr()->literal('%' .$search . '%'))
                        ));
                
            }
        }
            
        if ( null == $animalSearch || !array_key_exists('withLive', $animalSearch)) {
            $qb->andWhere('l.id IS NOT NULL');
        }
         
        if ( null !== $person){
            $qb->andWhere('o.id = :person')
                ->setParameter('person', $person->getId())
                 ;
            
        }
        
        $qb->orderBy('l.dateBegin', 'DESC');
        //$qb->addOrderBy('pl.name', 'ASC');
        //$qb->addOrderBy('r.name', 'ASC');
       // $qb->addOrderBy('p.dateTo', 'DESC');
        //$qb->addOrderBy('t.date', 'DESC');
        
        $animals = $qb->getQuery()
            ->getResult();
        
        return $animals;

        /*
        $taskRepository = new TaskRepository($this->em);
        $taskQb= $taskRepository->createQueryBuilder('t');
        $animalsTasks = $taskQb->andWhere('t.animal IN (:aIds)')
            ->setParameter('aIds', $animals)
            ->addOrderBy('t.date', 'DESC')
            ->getQuery()
            ->getResult()
            ;
        $arrayAnimalsTasks = [];
        foreach($animalsTasks as $animalTask){
            $arrayAnimalsTasks[$animalTask->getAnimal()->getId()][] = $animalTask;            
        }
        dd($arrayAnimalsTasks);
        foreach($animals as $animal){
            //dd($animal);
            if(key_exists($animal->getID(), $arrayAnimalsTasks)){
                foreach ($arrayAnimalsTasks[$animal->getID()] as $task) {
                    $animal->addTask($task);
                }
                
            }
        
        }
        dd($animals);
        */
    }
    


    // /**
    //  * @return Animal[] Returns an array of Animal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Animal
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
