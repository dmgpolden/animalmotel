<?php

namespace App\Repository;

use App\Entity\AnimalFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AnimalFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnimalFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnimalFile[]    findAll()
 * @method AnimalFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimalFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnimalFile::class);
    }

    // /**
    //  * @return AnimalFile[] Returns an array of AnimalFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnimalFile
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
