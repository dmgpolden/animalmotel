<?php

namespace App\Repository;

use App\Entity\Payment;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function mb_strtolower;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    /**
     * Find by any fileld with period
     * @param string search Field search for name owner o name animal
     * @param DateTime dateFrom Begin period search
     * @param DateTime dateTo End period search
     * @return Payment[] Returns an array of Payment objects
     */
    public function findWithDate(DateTime $dateFrom, DateTime $dateTo, string $search = null) 
    {
        //dd($dateFrom, $dateTo);
        $search= mb_strtolower($search);
        $qb= $this->createQueryBuilder('p');
        $qb->select(['p', 'a', 'o', 'l'])
            ->leftJoin('p.animal', 'a')
            ->leftJoin('a.owner', 'o')
            ->leftJoin('a.live', 'l')
            ;
           
        if ( null !== $search){
            $qb->add('where', $qb->expr()->orX(
                $qb->expr()->like($qb->expr()->lower('o.nameFirst'), $qb->expr()->literal('%' .$search . '%')),
                $qb->expr()->like($qb->expr()->lower('o.nameLast'), $qb->expr()->literal('%' .$search . '%')),
                $qb->expr()->like($qb->expr()->lower('a.name'), $qb->expr()->literal('%' .$search . '%'))
                ));
        }
            $qb->andWhere('p.date >= :dateFrom')
                ->andWhere('p.date <= :dateTo')
                ->setParameter('dateFrom', $dateFrom)  
                ->setParameter('dateTo', $dateTo);
            
            $qb->orderBy('p.date', 'DESC')
                ->addOrderBy('p.amount', 'ASC');
            
        return $qb->getQuery()->getResult();
    }
    

    /*
    public function findOneBySomeField($value): ?Payment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
