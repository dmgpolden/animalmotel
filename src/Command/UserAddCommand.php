<?php

namespace App\Command;

use App\Handler\UserHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAddCommand extends Command
{
    protected static $defaultName = 'user-add';
    private $passwordEncoder;

    public function __construct(UserHandler $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('email', InputArgument::OPTIONAL, 'Email')
            ->addArgument('password', InputArgument::OPTIONAL, 'Password')
            
        ;
    }
   
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $password= $input->getArgument('password');

        if (!$email) {
            $io->warning(sprintf('You passed an argument: %s', 'email'));
            return 0;
        }
        if (!$password) {
            $io->warning(sprintf('You passed an argument: %s', 'password'));
            return 0;
        }
        
        $this->user->load($email, $password);
        
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
