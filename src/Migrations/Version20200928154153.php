<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200928154153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE person DROP CONSTRAINT fk_34dcd176217bbb47');
        $this->addSql('DROP INDEX idx_34dcd176217bbb47');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_34DCD176217BBB47 ON person (person_id)');
        $this->addSql('ALTER TABLE live ADD price INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE live DROP price');
        $this->addSql('DROP INDEX UNIQ_34DCD176217BBB47');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT fk_34dcd176217bbb47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_34dcd176217bbb47 ON person (person_id)');
    }
}
