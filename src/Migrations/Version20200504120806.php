<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504120806 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE place (id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE person (id UUID NOT NULL, name_last VARCHAR(255) NOT NULL, name_first VARCHAR(255) NOT NULL, name_middle VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE contact (id UUID NOT NULL, type_id UUID NOT NULL, person_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4C62E638C54C8C93 ON contact (type_id)');
        $this->addSql('CREATE INDEX IDX_4C62E638217BBB47 ON contact (person_id)');
        $this->addSql('CREATE TABLE room (id UUID NOT NULL, place_id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_729F519BDA6A219 ON room (place_id)');
        $this->addSql('CREATE TABLE contact_type (id UUID NOT NULL, name VARCHAR(255) NOT NULL, required BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638C54C8C93 FOREIGN KEY (type_id) REFERENCES contact_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BDA6A219 FOREIGN KEY (place_id) REFERENCES place (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE breed ALTER id DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE room DROP CONSTRAINT FK_729F519BDA6A219');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638217BBB47');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638C54C8C93');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE contact_type');
        $this->addSql('ALTER TABLE breed ALTER id SET DEFAULT \'uuid_generate_v4()\'');
    }
}
