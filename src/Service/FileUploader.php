<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
//use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $targetDirectory;
    
    public function __construct($targetDirectory) 
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function ajaxUpload(UploadedFile $file)
    {
        $safeFilename = md5_file($file->getRealpath()); 
        $fileName = $safeFilename.'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
			throw new \Exception($e);
        }

        return $fileName;
    }

    public function upload(UploadedFile $file)
	{
        $safeFilename = md5_file($file->getRealpath()); 
        $fileName = $safeFilename.'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
			throw new \Exception($e);
        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}

