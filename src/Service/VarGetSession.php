<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use function dump;

/**
 * Get value from _GET or _SESSION
 *
 * @author dmg
 */
class VarGetSession {
    
    private $request;
    private $session;
    
    public function __construct(RequestStack $request, SessionInterface $session) {
        $this->request = $request->getCurrentRequest();
        $this->session = $session;
    }
    
    /**
     * Get value from _GET or _SESSION
     * 
     * @param string Key for get value 
     * @return string|array 
     */
    
    public function get($key) {
        $value = $this->session->get($key);
        
        if( $this->request->get($key) ){
            $value = $this->request->get($key);
            $this->session->set($key, $value );
        }
        
        if ( '' === $this->request->get($key) ){
            $value = $this->request->get($key);
            $this->session->set($key, null );
        }
        
        return $value;
        
    }
}
