<?php

namespace App\Service;

use App\Entity\File;

/**
 * Class DbUploadHandler
 * @author yourname
 */
class DbUploadHandler extends UploadHandler 
{

    private $animal;
    private $em;
    private $fileEntity;

    protected function initialize() {
        $this->em = $this->options['em'];
        $this->animal = $this->options['animal'];
        $this->fileEntity = $this->options['file'];
        parent::initialize();
    }

    protected function handle_form_data($file, $index) {
    	$file->title = @$_REQUEST['title'][$index];
    	$file->description = @$_REQUEST['description'][$index];
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
            $index = null, $content_range = null) {
        $file = parent::handle_file_upload(
        	$uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {
            $fileEntity = new File();
	        $fileEntity->setName($file->name);
	        $fileEntity->setSize($file->size);
	        $fileEntity->setType($file->type);
	        $fileEntity->setTitle($file->title);
            $fileEntity->setDescription($file->description);
        	$fileEntity->setAsType(0);
            $this->animal->addFile($fileEntity);
            $this->em->persist($fileEntity);
            $this->em->flush();
            $file->deleteUrl = '/file/ajax/file/'.$fileEntity->getId();
            $file->id = $fileEntity->getId();
        	$file->asType = $fileEntity->getAsType();
        }
        return $file;
    }
    
    protected function get_file_objects($iteration_method = 'get_file_object') {
        foreach ($this->animal->getFiles() as $_file) {
            $arrayFiles[] = $_file->getName();    
        }
        return array_values(array_filter(array_map(
            array($this, $iteration_method),
            $this->animal->getFiles()->toArray()
        )));
    }
    
    protected function get_file_object($fileEntity) {
        if ($this->is_valid_file_object($fileEntity->getName())) {
            $file = new \stdClass();
            $file->name = $fileEntity->getName();
            $file->size = $this->get_file_size(
                $this->get_upload_path($fileEntity->getName())
            );
            $file->url = $this->get_download_url($file->name);
            foreach ($this->options['image_versions'] as $version => $options) {
                if (!empty($version)) {
                    if (is_file($this->get_upload_path($fileEntity->getName(), $version))) {
                        $file->{$version.'Url'} = $this->get_download_url(
                            $file->name,
                            $version
                        );
                    }
                }
            }
            $this->set_additional_file_properties($file,$fileEntity);
            return $file;
        }
        return null;
    }

    protected function set_additional_file_properties($file,$fileEntity=null) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	        	$file->id = $fileEntity->getId();
        		$file->type = $fileEntity->getType();
        		$file->asType = $fileEntity->getAsType();
        		$file->title = $fileEntity->getTitle();
        		$file->description = $fileEntity->getDescription();
                $file->deleteUrl = '/file/ajax/file/'.$fileEntity->getId();
        }
    }

    public function delete($print_response = true) {
        $response = parent::delete(false);
        $_file = $this->fileEntity;
        $this->em->remove($this->fileEntity);
        $this->em->flush();

        $response = ['files' => [ $_file->getName() => true ] ];

        return $this->generate_response($response, $print_response);
    }

}

