    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    
    <script src="/js/doka.min.js"></script>
    <!-- blueimp Gallery script -->
    <script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="/js/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="/js/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="/js/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="/js/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="/js/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="/js/jquery.fileupload-validate.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="/js/jquery.fileupload-ui.js"></script>
<script type="text/javascript"> 
/* global $ */

$(function () {
  'use strict';

  // Initialize the jQuery File Upload widget:
  $('#fileupload').fileupload({
       // Setup Doka Image Editor:
    doka: Doka.create(),
    edit:
      Doka.supported() &&
      function (file) {
        return this.doka.edit(file).then(function (output) {
          return output && output.file;
        });
      },
    // Uncomment the following to send cross-domain cookies:
    //xhrFields: {withCredentials: true},
    url: '{{path("file_ajax", {"animal_id": animal_id} )}}',
    disableImageResize: /Android(?!.*Chrome)|Opera/
        .test(window.navigator && navigator.userAgent),
    imageMaxWidth: 800,
    imageMaxHeight: 800
  })
    .on('fileuploaddone', function (e, data) {
    })
    .on('fileuploadsubmit', function (e, data) {
       data.formData = data.context.find(':input').serializeArray();
    })
    ;
    

    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
      // Uncomment the following to send cross-domain cookies:
      //xhrFields: {withCredentials: true},
      url: $('#fileupload').fileupload('option', 'url'),
      dataType: 'json',
      context: $('#fileupload')[0]
    })
      .always(function () {
        $(this).removeClass('fileupload-processing');
      })
      .done(function (result) {
        $(this)
          .fileupload('option', 'done')
          // eslint-disable-next-line new-cap
          .call(this, $.Event('done'), { result: result });
      });
  

});
    
function asDocument(e){ 
    var animal = $(e).closest('form').data('animal');
    var file = $(e).data('file');
    $.ajax({
      // Uncomment the following to send cross-domain cookies:
      //xhrFields: {withCredentials: true},
    type: 'post',
    url: '{{path("animal_document", {"id": animal.id} ) }}',
    data: {file_id: file},
    })
      .done(function (result) {
          console.log(result);
        $(e).closest('tr').remove();
        $('#documents').attr('src', '/images/thumbnail/' + result.file );
      });
    return 'ok';
    };
function makeavatar(e){ 
    var animal = $(e).closest('form').data('animal');
    var file = $(e).data('file');
    $.ajax({
      // Uncomment the following to send cross-domain cookies:
      //xhrFields: {withCredentials: true},
    type: 'post',
    url: '{{path("animal_image", {"id": animal.id} ) }}',
    data: {file_id: file},
    })
      .done(function (result) {
        $(e).closest('tr').remove();
          console.log(result);
        $('img#avatar').attr('src', '/images/thumbnail/' + result.file );
      });
    return 'ok';
    };
</script>
